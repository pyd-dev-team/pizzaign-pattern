using System.Xml;
using System.Xml.Serialization;

namespace PizzaignPattern.Models.Reader;

public class XmlCommandReaderStrategy : ICommandReaderStrategy
{
    public Dictionary<string, List<Pizza>> ReadCommands(string filePath)
    {
        var serializer = new XmlSerializer(typeof(Dictionary<string, List<Pizza>>));
        using (var reader = XmlReader.Create(filePath))
        {
            var commands = (Dictionary<string, List<Pizza>>)serializer.Deserialize(reader);
            return commands;
        }
    }
}