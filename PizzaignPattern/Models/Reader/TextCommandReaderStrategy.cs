namespace PizzaignPattern.Models.Reader;

public class TextCommandReaderStrategy : ICommandReaderStrategy
{
    public Dictionary<string, List<Pizza>> ReadCommands(string filePath)
    {
        var commands = new Dictionary<string, List<Pizza>>();
        
        using (var reader = new StreamReader(filePath))
        {
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                // Parse the line into a command and add it to the commands
                // This will depend on your specific command structure
            }
        }
        
        return commands;
    }
}