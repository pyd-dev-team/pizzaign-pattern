using PizzaignPattern.utils;

namespace PizzaignPattern.Models;

public class CommandHandler
{
    public void HandleCommand(string command)
    {
        var orders = command.Split(',');

        // Dictionnaire pour stocker les pizzas et leurs quantités
        Dictionary<string, int> pizzaQuantities = new Dictionary<string, int>();
        Dictionary<string, Pizza> pizzaTypes = new Dictionary<string, Pizza>();

        foreach (var order in orders)
        {
            var parts = order.Trim().Split(new []{' '},2);
            var pizzaName = parts[1].ToLower();
            pizzaName = Utilities.RemoveDiacritics(pizzaName);
            var pizza = PizzaFactory.CreatePizza(pizzaName.Trim());
            int quantity = Int32.Parse(parts[0]);
            
            if (!pizzaQuantities.ContainsKey(pizza.Name.ToLower()))
            {
                pizzaQuantities.Add(pizza.Name.ToLower(), quantity);
                pizzaTypes.Add(pizza.Name.ToLower(), pizza);
            }
            else
            {
                pizzaQuantities[pizza.Name.ToLower()] += quantity;
            }
        }

        // Créer un dictionnaire fusionné pour passer aux commandes
        Dictionary<Pizza, int> pizzas = new Dictionary<Pizza, int>();
        foreach(var pizzaType in pizzaTypes)
        {
            pizzas.Add(pizzaType.Value, pizzaQuantities[pizzaType.Key]);
        }
        
        // Print la facture
        ICommand printInvoiceCommand = new PrintInvoiceCommand(pizzas);
        printInvoiceCommand.Execute();

        // Print la préparation des pizzas
        ICommand preparePizzaCommand = new PreparePizzaCommand(pizzas);
        preparePizzaCommand.Execute();
        
        // Print les ingredients et les pizza liés
        ICommand listIngredientsCommand = new ListIngredientsCommand(pizzas);
        listIngredientsCommand.Execute();
    }
}
