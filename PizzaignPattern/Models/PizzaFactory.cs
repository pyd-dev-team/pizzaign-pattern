namespace PizzaignPattern.Models;

public class PizzaFactory
{
    public static Pizza CreatePizza(string type)
    {
        //Chaque type de pizza doit être en minuscules et sans accents car traîté directement dans le code pour accepter les différentes orthographes
        return type switch
        {
            "regina" => new Pizza
            {
                Name = "Regina",
                Price = 8m,
                Ingredients = new Dictionary<string, int>
                {
                    { "150g de tomate", 1 },
                    { "125g de mozzarella", 1 },
                    { "100g de fromage râpé", 1 },
                    { "2 tranches de jambon", 1 },
                    { "4 champignons frais", 1 },
                    { "2 cuillères à soupe d’huile d’olive", 1 },
                }
            },
            "4 saisons" => new Pizza
            {
                Name = "4 saisons",
                Price = 9m,
                Ingredients = new Dictionary<string, int>
                {
                    { "150g de tomate", 1 },
                    { "125g de mozzarella", 1 },
                    { "2 tranches de jambon", 1 },
                    { "100g de champignons frais", 1 },
                    { "0,5 poivron", 1 },
                    { "1 poignée d'olives", 1 },
                }
            },
            "vegetarienne" => new Pizza
            {
                Name = "Végétarienne",
                Price = 7.50m,
                Ingredients = new Dictionary<string, int>
                {
                    { "150g de tomate", 1 },
                    { "100g de mozzarella", 1 },
                    { "0,5 courgette", 1 },
                    { "1 poivron jaune", 1 },
                    { "6 tomates cerises", 1 },
                    { "quelques olives", 1 },
                }
            },
            _ => throw new ArgumentException("Type de pizza inconnu")
        };
    }
}
