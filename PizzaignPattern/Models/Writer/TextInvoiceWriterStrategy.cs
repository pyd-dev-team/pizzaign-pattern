namespace PizzaignPattern.Models.Writer;

public class TextInvoiceWriterStrategy : IInvoiceWriterStrategy
{
    public void WriteInvoice(Dictionary<string, List<Pizza>> pizzas, string filePath)
    {
        using (var writer = new StreamWriter(filePath))
        {
            foreach (var pizza in pizzas)
            {
                // Write the invoice to the file
                // This will depend on your specific invoice structure
            }
        }
    }
}