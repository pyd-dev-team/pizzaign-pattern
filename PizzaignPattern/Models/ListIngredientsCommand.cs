namespace PizzaignPattern.Models;

public class ListIngredientsCommand : ICommand
{
    private Dictionary<Pizza, int> _pizzas;

    public ListIngredientsCommand(Dictionary<Pizza, int> pizzas)
    {
        _pizzas = pizzas;
    }

    public void Execute()
    {
        Dictionary<string, List<KeyValuePair<Pizza, int>>> ingredients = new Dictionary<string, List<KeyValuePair<Pizza, int>>>();

        foreach (var pizzaOrder in _pizzas)
        {
            for (int i = 0; i < pizzaOrder.Value; i++)
            {
                foreach (var ingredient in pizzaOrder.Key.Ingredients)
                {
                    if (!ingredients.ContainsKey(ingredient.Key))
                    {
                        ingredients[ingredient.Key] = new List<KeyValuePair<Pizza, int>>();
                    }
                    ingredients[ingredient.Key].Add(new KeyValuePair<Pizza, int>(pizzaOrder.Key, ingredient.Value));
                }
            }
        }

        foreach (var ingredient in ingredients)
        {
            Console.WriteLine($"{ingredient.Key} : {ingredient.Value.Count}");

            foreach (var pizza in ingredient.Value)
            {
                Console.WriteLine($"- {pizza.Key.Name} : {pizza.Value}");
            }
        }
        Console.WriteLine("");// Pour la lisibilité
    }
}