namespace PizzaignPattern.Models;

public class Order
{
    public Dictionary<string, int> Pizzas { get; set; }
    
    public Order(Dictionary<string, int> pizzas)
    {
        Pizzas = pizzas;
    }

}