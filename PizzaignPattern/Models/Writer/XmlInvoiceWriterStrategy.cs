using System.Xml;
using System.Xml.Serialization;

namespace PizzaignPattern.Models.Writer;

public class XmlInvoiceWriterStrategy : IInvoiceWriterStrategy
{
    public void WriteInvoice(Dictionary<string, List<Pizza>> pizzas, string filePath)
    {
        var serializer = new XmlSerializer(typeof(Dictionary<string, List<Pizza>>));
        using (var writer = XmlWriter.Create(filePath))
        {
            serializer.Serialize(writer, pizzas);
        }
    }
}