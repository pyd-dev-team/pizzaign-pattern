using Newtonsoft.Json;

namespace PizzaignPattern.Models.Reader;

public class JsonCommandReaderStrategy : ICommandReaderStrategy
{
    public Dictionary<string, List<Pizza>> ReadCommands(string filePath)
    {
        var json = File.ReadAllText(filePath);
        var commands = JsonConvert.DeserializeObject<Dictionary<string, List<Pizza>>>(json);
        return commands;
    }
}