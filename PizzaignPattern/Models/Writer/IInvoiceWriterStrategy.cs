namespace PizzaignPattern.Models.Writer;

public interface IInvoiceWriterStrategy
{
    void WriteInvoice(Dictionary<string, List<Pizza>> pizzas, string filePath);
}