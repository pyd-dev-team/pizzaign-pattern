namespace PizzaignPattern.Models;

public interface ICommand
{
    void Execute();
}