﻿namespace PizzaignPattern;
using Models;

public class Program
{
    private static void Main()
    {
        CommandHandler commandHandler = new CommandHandler();

        while (true)
        {
            Console.WriteLine("Entrez votre commande : ");
            var input = Console.ReadLine() ?? "";

            try
            {
                commandHandler.HandleCommand(input);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur : " + ex.Message);
            }
        }
    }
}