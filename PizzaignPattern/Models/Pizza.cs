namespace PizzaignPattern.Models;

public class Pizza
{
    public string Name { get; set; } = "";
    public Dictionary<string, int> Ingredients { get; set; } = new Dictionary<string, int>();
    public decimal Price { get; set; }

    public void Prepare()
    {
        Console.WriteLine($"Préparer la pâte");
        foreach (var ingredient in Ingredients)
        {
            Console.WriteLine($"Ajouter {ingredient.Key} {ingredient.Value}");
        }
        Console.WriteLine($"Cuire la pizza");
    }
}
