namespace PizzaignPattern.Models;

public class PrintInvoiceCommand : ICommand
{
    private readonly Dictionary<Pizza, int> _pizzas;

    public PrintInvoiceCommand(Dictionary<Pizza, int> pizzas)
    {
        _pizzas = pizzas;
    }

    public void Execute()
    {
        decimal totalPrice = 0;
        foreach (var pizzaOrder in _pizzas)
        {
            Console.WriteLine(); // Pour la lisibilité
            Console.WriteLine($"{pizzaOrder.Value} {pizzaOrder.Key.Name} : {pizzaOrder.Value} * {pizzaOrder.Key.Price}€");
            foreach (var ingredient in pizzaOrder.Key.Ingredients)
            {
                // Multipliez la quantité de chaque ingrédient par le nombre de pizzas commandées
                Console.WriteLine($"{ingredient.Key} {ingredient.Value * pizzaOrder.Value}");
            }
            totalPrice += pizzaOrder.Key.Price * pizzaOrder.Value;
        }
        Console.WriteLine($"Prix total : {totalPrice}€");
        
        Console.WriteLine(); // Pour la lisibilité
    }
}

public class PreparePizzaCommand : ICommand
{
    private readonly Dictionary<Pizza, int> _pizzas;

    public PreparePizzaCommand(Dictionary<Pizza, int> pizzas)
    {
        _pizzas = pizzas;
    }

    public void Execute()
    {
        foreach (var pizzaOrder in _pizzas)
        {
            for (int i = 0; i < pizzaOrder.Value; i++)
            {
                pizzaOrder.Key.Prepare();
                Console.WriteLine(); // Pour la lisibilité
            }
        }
    }
}

