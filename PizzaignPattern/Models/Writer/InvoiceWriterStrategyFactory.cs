namespace PizzaignPattern.Models.Writer;

public class InvoiceWriterStrategyFactory
{
    public IInvoiceWriterStrategy Create(string filePath)
    {
        string extension = Path.GetExtension(filePath);
        switch (extension)
        {
            case ".txt":
                return new TextInvoiceWriterStrategy();
            case ".json":
                return new JsonInvoiceWriterStrategy();
            case ".xml":
                return new XmlInvoiceWriterStrategy();
            default:
                throw new NotSupportedException($"File extension {extension} is not supported");
        }
    }
}