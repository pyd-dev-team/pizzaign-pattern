namespace PizzaignPattern.Models.Reader;

public interface ICommandReaderStrategy
{
    Dictionary<string, List<Pizza>> ReadCommands(string filePath);
}