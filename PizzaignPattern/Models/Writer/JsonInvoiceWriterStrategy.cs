using Newtonsoft.Json;

namespace PizzaignPattern.Models.Writer;

public class JsonInvoiceWriterStrategy : IInvoiceWriterStrategy
{
    public void WriteInvoice(Dictionary<string, List<Pizza>> pizzas, string filePath)
    {
        var json = JsonConvert.SerializeObject(pizzas, Formatting.Indented);
        File.WriteAllText(filePath, json);
    }
}