namespace PizzaignPattern.Models.Reader;

public class CommandReaderStrategyFactory
{
    public ICommandReaderStrategy Create(string filePath)
    {
        string extension = Path.GetExtension(filePath);
        switch (extension)
        {
            case ".txt":
                return new TextCommandReaderStrategy();
            case ".json":
                return new JsonCommandReaderStrategy();
            case ".xml":
                return new XmlCommandReaderStrategy();
            default:
                throw new NotSupportedException($"Le format de fichier {extension} n'est pas supporté");
        }
    }
}